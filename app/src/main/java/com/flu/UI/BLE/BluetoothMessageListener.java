package com.flu.UI.BLE;


import com.flu.UI.MessagePacket;

public interface BluetoothMessageListener {
    void onMessageReceived(MessagePacket messagePacket);
    void onMessageError(String partMessage);
}
