package com.flu.UI.BLE;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.flu.R;
import com.flu.UI.MessagePacket;
import com.google.android.gms.common.util.Strings;


public class BluetoothConnected extends Fragment  {
    private BluetoothUtils mBluetoothUtils = BluetoothUtils.getInstance();
    private EditText mEtText;
    private TextView mTxtView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_bt_connect, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBluetoothUtils.registerMessageListener(new BluetoothMessageListener() {
            @Override
            public void onMessageReceived(MessagePacket messagePacket) {
                displayData(messagePacket.getData());
                respond(messagePacket.getData());
            }

            @Override
            public void onMessageError(String partMessage) {
                displayData(partMessage);
            }
        });
        mTxtView = view.findViewById(R.id.txtvMessage);


        mEtText = view.findViewById(R.id.etText);
        view.findViewById(R.id.btnSend).setOnClickListener(v ->
                sendMessage(mEtText.getText().toString()));
    }

    private void respond(String message){
//        mBluetoothUtils.sendMessage()
    }

    private void displayData(String message) {
        new Handler(Looper.getMainLooper()).post(() -> mTxtView.setText(message));
    }

    private void sendMessage(@NonNull String message) {
        if (!Strings.isEmptyOrWhitespace(message)) {
            MessagePacket messagePacket = new MessagePacket(message);
            boolean isSend = mBluetoothUtils.sendMessage(messagePacket);
            if (isSend) {
                mEtText.getText().clear();
            } else {
                Toast.makeText(getContext(), R.string.problem_with_sending,
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            mEtText.setError(getString(R.string.et_error));
        }
    }
}
