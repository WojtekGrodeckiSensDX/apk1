package com.flu.UI.MENU_DRAWER;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.flu.R;
import com.flu.UI.BLE.BluetoothActivity;

public class Instruction extends AppCompatActivity {
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);





        dl = (DrawerLayout) findViewById(R.id.activity_main);
        t = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);

        dl.addDrawerListener(t);
        t.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nv = (NavigationView) findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                // Handle navigation view item clicks here.
                int id = item.getItemId();

                if (id == R.id.HOME) {
                    startHome();
                    Toast.makeText(Instruction.this, "Nowy Test", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.INSTRUCTION) {
                    startInstruction();
                    Toast.makeText(Instruction.this, "Instrukcja", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.CONNECTED) {
                    startConnected();
                    Toast.makeText(Instruction.this, "Połączone urządzenia", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.HISTORY) {
                    startHistory();
                    Toast.makeText(Instruction.this, "Historia", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.ABOUT) {
                    startAbout();
                    Toast.makeText(Instruction.this, "O Sens Dx", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.TEST) {
                    startTest();
                    Toast.makeText(Instruction.this, "Test", Toast.LENGTH_SHORT).show();
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }

            private void startHome() {
                Intent intent = new Intent(Instruction.this, NewTest.class);
                startActivity(intent);
            }

            private void startInstruction() {
                Intent intent = new Intent(Instruction.this, Instruction.class);
                startActivity(intent);
            }

            private void startConnected() {
                Intent intent = new Intent(Instruction.this, Connected.class);
                startActivity(intent);
            }

            private void startHistory() {
                Intent intent = new Intent(Instruction.this, History.class);
                startActivity(intent);
            }

            private void startAbout() {
                Intent intent = new Intent(Instruction.this, About.class);
                startActivity(intent);
            }

            private void startTest() {
                Intent intent = new Intent(Instruction.this, Test.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }


}
