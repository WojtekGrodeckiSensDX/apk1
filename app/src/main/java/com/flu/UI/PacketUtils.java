package com.flu.UI;

import android.support.annotation.NonNull;


import java.io.InvalidObjectException;
import java.io.UnsupportedEncodingException;

public class PacketUtils {
    private static final int ERROR_INDEX = -1;
    private static final String PACKET_START = "<";
    public static final String PACKET_END = ">";
    private static final String PACKET_SEPARATOR = "|";

    public @NonNull String parse(@NonNull MessagePacket messagePacket) {
        return PACKET_START +
                messagePacket.getPayload() +
                PACKET_SEPARATOR +
                messagePacket.getData() +
                PACKET_SEPARATOR +
                messagePacket.getCrc16() +
                PACKET_END;
    }

    public @NonNull MessagePacket parse(@NonNull String message)
            throws UnsupportedEncodingException, InvalidObjectException {
        int packetStart = message.indexOf(PACKET_START);
        int firstSeparator = message.indexOf(PACKET_SEPARATOR);
        int secondSeparator = message.indexOf(PACKET_SEPARATOR, firstSeparator + 1);
        int packetEnd = message.indexOf(PACKET_END);

        if (packetStart == ERROR_INDEX) {
            throw new InvalidObjectException("Packet start not found");
        } else if (firstSeparator == ERROR_INDEX) {
            throw new InvalidObjectException("First separator not found");
        } else if (secondSeparator == ERROR_INDEX) {
            throw new InvalidObjectException("Second separator not found");
        } else if (packetEnd == ERROR_INDEX) {
            throw new InvalidObjectException("Packet end not found");
        }

        int payload;
        try {
            payload = Integer.parseInt(message.substring(packetStart + 1, firstSeparator));
        } catch (NumberFormatException e) {
            throw new InvalidObjectException("Payload not found");
        }
        String data = message.substring(firstSeparator + 1, secondSeparator);
        String crc16 = message.substring(secondSeparator + 1, packetEnd);

        return new MessagePacket(payload, data.getBytes(), crc16);
    }
}
