package com.flu.UI.BLE;
import android.support.annotation.NonNull;

import java.util.List;

public interface BluetoothScanListener {
    void onScanCompleted(@NonNull List<com.flu.UI.BLE.BluetoothDeviceListItem> list);
}
